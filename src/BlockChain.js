import pkg from 'crypto-js'
import Block from './Block.js';

class BlockChain {
    constructor() {
        this.chain = []
        this.height = -1;
        this.initializeChain()
    }

    async initializeChain() {
        //creamos el primero bloque genesis por eso es el condicional
        //saber si es exactamente igual a -1 se creara el bloque genesis
        if (this.height === -1) {
            const block = new Block({
                data: "Genesis Block"
            })
            await this.addBlock(block)
        }
    }

    addBlock(block) {
        // se rellenan los datos del bloque
        return new Promise(async (resolve, reject) => {
            block.height = this.chain.length
            block.time = new Date().getTime().toString()

            //si la cadena tiene mayor a 0 bloques quiere decir que existen
            //y vamos añadir al bloque nuevo el hash del bloque anterior
            if (this.chain.length > 0) {
                block.previeusBlockHash = this.chain[this.chain.length - 1].hash
            }

            //el metodo lo que hace es llamar al metodo validate que se encuentra
            // en la clase Block
            let errors = await this.validateChain()
            if (errors.length > 0) {
                reject(new Error('This chain is invalid:', errors))
            }

            const {
                SHA256
            } = pkg
            block.hash = SHA256(JSON.stringify(block)).toString()
            this.chain.push(block)
            resolve(block)
        })
    }

    validateChain() {
        const errors = []
        return new Promise((resolve) => {
            this.chain.map(async (block) => {
                try {
                    const isValid = await block.validate()
                    if (!isValid) {
                        errors.push(new Error(`The block ${block.height} is invalid`))
                    }
                } catch (error) {
                    errors.push(error)
                }
            })
            resolve(errors)
        })
    }

    print() {
        //recorrer toda la chain para imprimir el bloque con su metodo toString()
        for (let block of this.chain) {
            console.log(block.toString())
        }
    }
}

export default BlockChain
import pkg from 'crypto-js'
import hex2ascii from 'hex2ascii'

class Block {
    constructor(data) {
        this.hash = null
        this.height = 0
        this.body = Buffer.from(JSON.stringify(data).toString('hex'))
        this.time = 0
        this.previeusBlockHash = null
    }

    validate() {
        //validar el bloque para saber si tuvo un hackeo
        return new Promise((resolve) => {
            let currentHash = this.hash

            const { SHA256 } = pkg
            this.hash = SHA256(JSON.stringify({
                ...this,
                hash: null
            })).toString()

            if (currentHash != this.hash) {
                resolve(false)
            }

            resolve(true)
        })
    }

    getBlockData() {
        //obtener el encode de la data del bloque y decodificar y parasearlo a un obj JS
        return new Promise((resolve, reject) => {
            const encodeData = this.body // -> hex
            const decodeData = hex2ascii(encodeData) // -> string
            const dataObject = JSON.parse(decodeData)

            // Genesis Block: es el primer objeto que se crea en la blockchain
            if (dataObject == 'Genesis Block') {
                reject(new Error('This is the Genesis Block'))
            }

            resolve(dataObject)
        })
    }

    toString() {
        //Mostrar el contenido del bloque en un string
        const {
            hash,
            height,
            body,
            time,
            previeusBlockHash
        } = this
        return `Block:
             hash: ${hash}
             height: ${height}
             body: ${body}
             time: ${time}
             previeusBlockHash: ${previeusBlockHash}
        `
    }
}

export default Block